# 实验3：创建分区表

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
- 进行分区与不分区的对比实验。

## 实验参考

- 使用sql-developer软件创建表，并导出类似以下的脚本。
- 以下脚本不含orders.customer_name的索引，不含序列设置，仅供参考。

```sql
CREATE TABLE orders 
(
 order_id NUMBER(9, 0) NOT NULL
 , customer_name VARCHAR2(40 BYTE) NOT NULL 
 , customer_tel VARCHAR2(40 BYTE) NOT NULL 
 , order_date DATE NOT NULL 
 , employee_id NUMBER(6, 0) NOT NULL 
 , discount NUMBER(8, 2) DEFAULT 0 
 , trade_receivable NUMBER(8, 2) DEFAULT 0 
 , CONSTRAINT ORDERS_PK PRIMARY KEY 
  (
    ORDER_ID 
  )
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE (   BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL 

PARTITION BY RANGE (order_date) 
(
 PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
 TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
 'NLS_CALENDAR=GREGORIAN')) 
 NOLOGGING
 TABLESPACE USERS
 PCTFREE 10 
 INITRANS 1 
 STORAGE 
( 
 INITIAL 8388608 
 NEXT 1048576 
 MINEXTENTS 1 
 MAXEXTENTS UNLIMITED 
 BUFFER_POOL DEFAULT 
) 
NOCOMPRESS NO INMEMORY  
, PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING
TABLESPACE USERS
, PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING 
TABLESPACE USERS
);
--以后再逐年增加新年份的分区
ALTER TABLE orders ADD PARTITION partition_before_2022
VALUES LESS THAN(TO_DATE('2022-01-01','YYYY-MM-DD'))
TABLESPACE USERS;

```

- 创建order_details表的语句如下：

```sql
CREATE TABLE order_details
(
id NUMBER(9, 0) NOT NULL 
, order_id NUMBER(10, 0) NOT NULL
, product_id VARCHAR2(40 BYTE) NOT NULL 
, product_num NUMBER(8, 2) NOT NULL 
, product_price NUMBER(8, 2) NOT NULL 
, CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
  (
    id 
  )
, CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
REFERENCES orders  (  order_id   )
ENABLE
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);
```

- 创建序列SEQ1的语句如下

```sql
CREATE SEQUENCE  SEQ1  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
```

- 插入100条orders记录的样例脚本如下：

```sql
declare 
   i integer;
   y integer;
   m integer;
   d integer;
   str varchar2(100);
BEGIN  
  i:=0;
  y:=2015;
  m:=1;
  d:=12;
  while i<100 loop
    i := i+1;
    --在这里改变y,m,d
    m:=m+1;
    if m>12 then
        m:=1;
    end if;
    str:=y||'-'||m||'-'||d;
    insert into orders(order_id,order_date) 
      values(SEQ1.nextval,to_date(str,'yyyy-MM-dd'));
  end loop;
  commit;
END;
/

说明：
|| 表示字符连接符号
SEQ1是一个序列对象
```

## 实验步骤
- 创建 orders 表和 order_details 表
```sql
CREATE TABLE orders 
(
    order_id NUMBER(9, 0) NOT NULL,
    customer_name VARCHAR2(40 BYTE) NOT NULL,
    customer_tel VARCHAR2(40 BYTE) NOT NULL,
    order_date DATE NOT NULL,
    employee_id NUMBER(6, 0) NOT NULL,
    discount NUMBER(8, 2) DEFAULT 0,
    trade_receivable NUMBER(8, 2) DEFAULT 0,
    CONSTRAINT ORDERS_PK PRIMARY KEY (order_id)
)
PARTITION BY RANGE (order_date)
(
    PARTITION ORDERS_2018 VALUES LESS THAN (TO_DATE('2019-01-01', 'yyyy-mm-dd')),
    PARTITION ORDERS_2019 VALUES LESS THAN (TO_DATE('2020-01-01', 'yyyy-mm-dd')),
    PARTITION ORDERS_2020 VALUES LESS THAN (TO_DATE('2021-01-01', 'yyyy-mm-dd')),
    PARTITION ORDERS_2021 VALUES LESS THAN (TO_DATE('2021-07-01', 'yyyy-mm-dd')),
    PARTITION ORDERS_MAX VALUES LESS THAN (MAXVALUE)
);

CREATE INDEX ORDERS_CUST_NAME_IDX ON orders (customer_name) TABLESPACE EXAMPLE;

CREATE TABLE order_details
(
    id NUMBER(10, 0) NOT NULL,
    order_id NUMBER(9, 0) NOT NULL,
    product_id NUMBER(6, 0) NOT NULL,
    quantity NUMBER(8, 2) NOT NULL,
    unit_price NUMBER(8, 2) NOT NULL,
    CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY (id),
    CONSTRAINT ORDER_DETAILS_FK1 FOREIGN KEY (order_id) REFERENCES orders(order_id)
)
PARTITION BY REFERENCE (ORDER_DETAILS_FK1);
```
- 创建序列并添加索引
```sql
CREATE SEQUENCE orders_seq 
MINVALUE 1 
MAXVALUE 9999999999 
INCREMENT BY 1 
START WITH 1 
NOCACHE 
NOCYCLE;

CREATE INDEX ORDERS_ID_IDX ON orders (order_id) TABLESPACE EXAMPLE;

CREATE SEQUENCE order_details_seq 
MINVALUE 1 
MAXVALUE 9999999999 
INCREMENT BY 1 
START WITH 1 
NOCACHE 
NOCYCLE;

CREATE INDEX ORDER_DETAILS_ID_IDX ON order_details (id) TABLESPACE EXAMPLE;
```
- 插入数据到 orders 和 order_details 表
```sql
DECLARE
    DISCOUNTS NUMBER(2, 2);
    i NUMBER(6);
    j NUMBER(6);
BEGIN
    FOR i IN 1..400 LOOP

        INSERT INTO orders (order_id, customer_name, customer_tel, order_date, employee_id, discount, trade_receivable) 
        VALUES (orders_seq.NEXTVAL, 'cust_name'||i, '1390000000'||i, 
                to_date('2019-'||'02'||'-'||TRUNC(DBMS_RANDOM.VALUE(1, 28))||' 09:26:03', 'yyyy-mm-dd hh24:mi:ss'), 
                TRUNC(DBMS_RANDOM.VALUE(1000, 9999)), 
                ROUND(DBMS_RANDOM.VALUE(0, 0.4), 2), 
                ROUND(DBMS_RANDOM.VALUE(1000, 10000), 2));

        FOR j IN 1..5 LOOP
            DISCOUNTS:= ROUND(DBMS_RANDOM.VALUE(0, 0.3), 2);
            INSERT INTO order_details (id, order_id, product_id, quantity, unit_price)
            VALUES (order_details_seq.NEXTVAL, i, TRUNC(DBMS_RANDOM.VALUE(1, 50)), TRUNC(DBMS_RANDOM.VALUE(1, 100)), 
                    ROUND(DBMS_RANDOM.VALUE(10, 1000), 2) * DISCOUNTS);
        END LOOP;
    END LOOP;
    COMMIT;
END;
/
```
- 执行联合查询，并分析执行计划
```sql
SELECT o.order_id, o.customer_name, d.id, d.product_id, d.quantity, d.unit_price
FROM orders o
JOIN order_details d ON (o.order_id = d.order_id)
WHERE o.customer_name = 'cust_name1';

EXPLAIN PLAN FOR 
SELECT o.order_id, o.customer_name, d.id, d.product_id, d.quantity, d.unit_price
FROM orders o
JOIN order_details d ON (o.order_id = d.order_id)
WHERE o.customer_name = 'cust_name1';
SELECT * FROM table(DBMS_XPLAN.DISPLAY);
```

- 运行结果

![3_1](./3_1.jpg)


![3_2](./3_2.jpg)

## 实验注意事项

- 完成时间：2023-05-08，请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的oracle项目中的test3目录中。
- 实验分析及结果文档说明书用Markdown格式编写。
