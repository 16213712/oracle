CREATE OR REPLACE PACKAGE MyPack IS
  FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
  PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END;
/

CREATE OR REPLACE PACKAGE BODY MyPack IS
  FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER IS
    N NUMBER(20,2);
  BEGIN
    SELECT SUM(salary) INTO N  FROM employees E WHERE E.department_id = V_DEPARTMENT_ID;
    RETURN N;
  END;

  PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER) IS
    LEFTSPACE VARCHAR(2000);
  BEGIN
    LEFTSPACE := ' ';
    FOR v IN
      (SELECT LEVEL, employee_id, first_name, manager_id FROM employees
      START WITH employee_id = V_EMPLOYEE_ID
      CONNECT BY PRIOR employee_id = manager_id)
    LOOP
      DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(v.LEVEL-1)*4,' ')||v.employee_id||' '||v.first_name);
    END LOOP;
  END;
END;
/

