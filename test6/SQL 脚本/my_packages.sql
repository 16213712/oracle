-- 声明
CREATE OR REPLACE PACKAGE my_package IS
  -- 插入数据的存储过程
  PROCEDURE insert_customer(p_id IN NUMBER, p_first_name IN VARCHAR2, p_last_name IN VARCHAR2, p_email IN VARCHAR2, p_phone IN VARCHAR2, p_address IN VARCHAR2);
  
  PROCEDURE insert_product(p_id IN NUMBER, p_name IN VARCHAR2, p_description IN VARCHAR2, p_price IN NUMBER);
  
  PROCEDURE insert_order(p_id IN NUMBER, p_customer_id IN NUMBER, p_order_date IN DATE, p_total IN NUMBER);
  
  PROCEDURE insert_order_item(p_id IN NUMBER, p_order_id IN NUMBER, p_product_id IN NUMBER, p_quantity IN NUMBER, p_price IN NUMBER);
  
  PROCEDURE insert_shipment(p_id IN NUMBER, p_order_id IN NUMBER, p_shipped_date IN DATE, p_carrier IN VARCHAR2, p_tracking_number IN VARCHAR2);
  
  PROCEDURE insert_payment(p_id IN NUMBER, p_order_id IN NUMBER, p_payment_date IN DATE, p_amount IN NUMBER);
  
  -- 获取数据的函数
  FUNCTION get_customers RETURN SYS_REFCURSOR;
  
  FUNCTION get_products RETURN SYS_REFCURSOR;
  
  FUNCTION get_orders RETURN SYS_REFCURSOR;
  
  FUNCTION get_order_items RETURN SYS_REFCURSOR;
  
  FUNCTION get_shipments RETURN SYS_REFCURSOR;
  
  FUNCTION get_payments RETURN SYS_REFCURSOR;
  
  -- 删除数据的存储过程
  PROCEDURE delete_customer(p_id IN NUMBER);
  
  PROCEDURE delete_product(p_id IN NUMBER);
  
  PROCEDURE delete_order(p_id IN NUMBER);
  
  PROCEDURE delete_order_item(p_id IN NUMBER);
  
  PROCEDURE delete_shipment(p_id IN NUMBER);
  
  PROCEDURE delete_payment(p_id IN NUMBER);
  
  -- 更新数据的存储过程
  PROCEDURE update_customer(p_id IN NUMBER, p_first_name IN VARCHAR2, p_last_name IN VARCHAR2, p_email IN VARCHAR2, p_phone IN VARCHAR2, p_address IN VARCHAR2);
  
  PROCEDURE update_product(p_id IN NUMBER, p_name IN VARCHAR2, p_description IN VARCHAR2, p_price IN NUMBER);
  
  PROCEDURE update_order(p_id IN NUMBER, p_customer_id IN NUMBER, p_order_date IN DATE, p_total IN NUMBER);
  
  PROCEDURE update_order_item(p_id IN NUMBER, p_order_id IN NUMBER, p_product_id IN NUMBER, p_quantity IN NUMBER, p_price IN NUMBER);
  
  PROCEDURE update_shipment(p_id IN NUMBER, p_order_id IN NUMBER, p_shipped_date IN DATE, p_carrier IN VARCHAR2, p_tracking_number IN VARCHAR2);
  
  PROCEDURE update_payment(p_id IN NUMBER, p_order_id IN NUMBER, p_payment_date IN DATE, p_amount IN NUMBER);
END my_package;
/



-- 实现
CREATE OR REPLACE PACKAGE BODY my_package IS
  PROCEDURE insert_customer(p_id IN NUMBER, p_first_name IN VARCHAR2, p_last_name IN VARCHAR2, p_email IN VARCHAR2, p_phone IN VARCHAR2, p_address IN VARCHAR2) IS
  BEGIN
    INSERT INTO customers (customer_id, first_name, last_name, email, phone, address) VALUES (p_id, p_first_name, p_last_name, p_email, p_phone, p_address);
    COMMIT;
  END;
  
  PROCEDURE insert_product(p_id IN NUMBER, p_name IN VARCHAR2, p_description IN VARCHAR2, p_price IN NUMBER) IS
  BEGIN
    INSERT INTO products (product_id, name, description, price) VALUES (p_id, p_name, p_description, p_price);
    COMMIT;
  END;
  
  PROCEDURE insert_order(p_id IN NUMBER, p_customer_id IN NUMBER, p_order_date IN DATE, p_total IN NUMBER) IS
  BEGIN
    INSERT INTO orders (order_id, customer_id, order_date, total) VALUES (p_id, p_customer_id, p_order_date, p_total);
    COMMIT;
  END;
  
  PROCEDURE insert_order_item(p_id IN NUMBER, p_order_id IN NUMBER, p_product_id IN NUMBER, p_quantity IN NUMBER, p_price IN NUMBER) IS
  BEGIN
    INSERT INTO order_items (order_item_id, order_id, product_id, quantity, price) VALUES (p_id, p_order_id, p_product_id, p_quantity, p_price);
    COMMIT;
  END;
  
  PROCEDURE insert_shipment(p_id IN NUMBER, p_order_id IN NUMBER, p_shipped_date IN DATE, p_carrier IN VARCHAR2, p_tracking_number IN VARCHAR2) IS
  BEGIN
    INSERT INTO shipments (shipment_id, order_id, shipped_date, carrier, tracking_number) VALUES (p_id, p_order_id, p_shipped_date, p_carrier, p_tracking_number);
    COMMIT;
  END;
  
  PROCEDURE insert_payment(p_id IN NUMBER, p_order_id IN NUMBER, p_payment_date IN DATE, p_amount IN NUMBER) IS
  BEGIN
    INSERT INTO payments (payment_id, order_id, payment_date, amount) VALUES (p_id, p_order_id, p_payment_date, p_amount);
    COMMIT;
  END;
  
  -- 获取数据的函数
  FUNCTION get_customers RETURN SYS_REFCURSOR IS
    r_cursor SYS_REFCURSOR;
  BEGIN
    OPEN r_cursor FOR
      SELECT * FROM customers;
    RETURN r_cursor;
  END;
  
  FUNCTION get_products RETURN SYS_REFCURSOR IS
    r_cursor SYS_REFCURSOR;
  BEGIN
    OPEN r_cursor FOR
      SELECT * FROM products;
    RETURN r_cursor;
  END;
  
  FUNCTION get_orders RETURN SYS_REFCURSOR IS
    r_cursor SYS_REFCURSOR;
  BEGIN
    OPEN r_cursor FOR
      SELECT * FROM orders;
    RETURN r_cursor;
  END;
  
  FUNCTION get_order_items RETURN SYS_REFCURSOR IS
    r_cursor SYS_REFCURSOR;
  BEGIN
    OPEN r_cursor FOR
      SELECT * FROM order_items;
    RETURN r_cursor;
  END;
  
  FUNCTION get_shipments RETURN SYS_REFCURSOR IS
    r_cursor SYS_REFCURSOR;
  BEGIN
    OPEN r_cursor FOR
      SELECT * FROM shipments;
    RETURN r_cursor;
  END;
  
  FUNCTION get_payments RETURN SYS_REFCURSOR IS
    r_cursor SYS_REFCURSOR;
  BEGIN
    OPEN r_cursor FOR
      SELECT * FROM payments;
    RETURN r_cursor;
  END;
  
  -- 删除数据的存储过程
  PROCEDURE delete_customer(p_id IN NUMBER) IS
  BEGIN
    DELETE FROM customers WHERE customer_id = p_id;
    COMMIT;
  END;
  
  PROCEDURE delete_product(p_id IN NUMBER) IS
  BEGIN
    DELETE FROM products WHERE product_id = p_id;
    COMMIT;
  END;
  
  PROCEDURE delete_order(p_id IN NUMBER) IS
  BEGIN
    DELETE FROM orders WHERE order_id = p_id;
    COMMIT;
  END;
  
  PROCEDURE delete_order_item(p_id IN NUMBER) IS
  BEGIN
    DELETE FROM order_items WHERE order_item_id = p_id;
    COMMIT;
  END;
  
  PROCEDURE delete_shipment(p_id IN NUMBER) IS
  BEGIN
    DELETE FROM shipments WHERE shipment_id = p_id;
    COMMIT;
  END;
  
  PROCEDURE delete_payment(p_id IN NUMBER) IS
  BEGIN
    DELETE FROM payments WHERE payment_id = p_id;
    COMMIT;
  END;
  
  -- 更新数据的存储过程
  PROCEDURE update_customer(p_id IN NUMBER, p_first_name IN VARCHAR2, p_last_name IN VARCHAR2, p_email IN VARCHAR2, p_phone IN VARCHAR2, p_address IN VARCHAR2) IS
  BEGIN
    UPDATE customers SET first_name = p_first_name, last_name = p_last_name, email = p_email, phone = p_phone, address = p_address WHERE customer_id = p_id;
    COMMIT;
  END;
  
  PROCEDURE update_product(p_id IN NUMBER, p_name IN VARCHAR2, p_description IN VARCHAR2, p_price IN NUMBER) IS
  BEGIN
    UPDATE products SET name = p_name, description = p_description, price = p_price WHERE product_id = p_id;
    COMMIT;
  END;
  
  PROCEDURE update_order(p_id IN NUMBER, p_customer_id IN NUMBER, p_order_date IN DATE, p_total IN NUMBER) IS
  BEGIN
    UPDATE orders SET customer_id = p_customer_id, order_date = p_order_date, total = p_total WHERE order_id = p_id;
    COMMIT;
  END;
  
  PROCEDURE update_order_item(p_id IN NUMBER, p_order_id IN NUMBER, p_product_id IN NUMBER, p_quantity IN NUMBER, p_price IN NUMBER) IS
  BEGIN
    UPDATE order_items SET order_id = p_order_id, product_id = p_product_id, quantity = p_quantity, price = p_price WHERE order_item_id = p_id;
    COMMIT;
  END;
  
  PROCEDURE update_shipment(p_id IN NUMBER, p_order_id IN NUMBER, p_shipped_date IN DATE, p_carrier IN VARCHAR2, p_tracking_number IN VARCHAR2) IS
  BEGIN
    UPDATE shipments SET order_id = p_order_id, shipped_date = p_shipped_date, carrier = p_carrier, tracking_number = p_tracking_number WHERE shipment_id = p_id;
    COMMIT;
  END;
  
  PROCEDURE update_payment(p_id IN NUMBER, p_order_id IN NUMBER, p_payment_date IN DATE, p_amount IN NUMBER) IS
  BEGIN
    UPDATE payments SET order_id = p_order_id, payment_date = p_payment_date, amount = p_amount WHERE payment_id = p_id;
    COMMIT;
  END;
END my_package;
/

