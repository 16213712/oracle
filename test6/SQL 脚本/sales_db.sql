-- 创建一个新的用户
 CREATE USER sales_user IDENTIFIED BY "123456";
-- DROP USER sales_user CASCADE;

-- 授予该用户所需的权限
 GRANT CREATE SESSION TO sales_user;
 GRANT CREATE TABLE TO sales_user;
 GRANT CREATE PROCEDURE TO sales_user;
 
 -- 创建两个表空间 ts1 和 ts2
 CREATE TABLESPACE ts1 DATAFILE 'ts1.dbf' SIZE 1G;
 CREATE TABLESPACE ts2 DATAFILE 'ts2.dbf' SIZE 1G;

-- 创建下面这六个表
-- 在表空间 ts1 中创建 customers 表
CREATE TABLE customers (
    customer_id NUMBER(10) PRIMARY KEY,
    first_name VARCHAR2(50),
    last_name VARCHAR2(50),
    email VARCHAR2(100),
    phone VARCHAR2(20),
    address VARCHAR2(200)
) TABLESPACE ts1;

-- 在表空间 ts1 中创建 products 表
CREATE TABLE products (
    product_id NUMBER(10) PRIMARY KEY,
    name VARCHAR2(100),
    description VARCHAR2(200),
    price NUMBER(10, 2)
) TABLESPACE ts1;

-- 在表空间 ts1 中创建 orders 表
CREATE TABLE orders (
    order_id NUMBER(10) PRIMARY KEY,
    customer_id NUMBER(10),
    order_date DATE,
    total NUMBER(10, 2),
    CONSTRAINT fk_customer
        FOREIGN KEY (customer_id)
        REFERENCES customers(customer_id)
) TABLESPACE ts1;

-- 在表空间 ts2 中创建 order_items 表
CREATE TABLE order_items (
    order_item_id NUMBER(10) PRIMARY KEY,
    order_id NUMBER(10),
    product_id NUMBER(10),
    quantity NUMBER(10),
    price NUMBER(10, 2),
    CONSTRAINT fk_order
        FOREIGN KEY (order_id)
        REFERENCES orders(order_id),
    CONSTRAINT fk_product
        FOREIGN KEY (product_id)
        REFERENCES products(product_id)
) TABLESPACE ts2;

-- 在表空间 ts2 中创建 shipments 表
CREATE TABLE shipments (
    shipment_id NUMBER(10) PRIMARY KEY,
    order_id NUMBER(10),
    shipped_date DATE,
    carrier VARCHAR2(50),
    tracking_number VARCHAR2(50),
    CONSTRAINT fk_order_shipment
        FOREIGN KEY (order_id)
        REFERENCES orders(order_id)
) TABLESPACE ts2;

-- 在表空间 ts2 中创建 payments 表
CREATE TABLE payments (
    payment_id NUMBER(10) PRIMARY KEY,
    order_id NUMBER(10),
    payment_date DATE,
    amount NUMBER(10, 2),
    CONSTRAINT fk_order_payment
        FOREIGN KEY (order_id)
        REFERENCES orders(order_id)
) TABLESPACE ts2;

-- 生成 customers 表的模拟数据
INSERT INTO customers(customer_id, first_name, last_name, email, phone, address)
SELECT 
    rownum, 
    'First' || rownum, 
    'Last' || rownum, 
    'email' || rownum || '@example.com', 
    '123-456-' || LPAD(rownum, 4, '0'), 
    'Address ' || rownum || ', City, State, Zip'
FROM 
    dual 
CONNECT BY 
    level <= 50000;

-- 生成 products 表的模拟数据
INSERT INTO products(product_id, name, description, price)
SELECT 
    rownum, 
    'Product ' || rownum, 
    'Description for product ' || rownum, 
    TRUNC(DBMS_RANDOM.VALUE(10, 1000), 2) 
FROM 
    DUAL 
CONNECT BY 
    level <= 50000;

-- 生成 orders 表的模拟数据
INSERT INTO orders(order_id, customer_id, order_date, total)
SELECT 
    rownum, 
    TRUNC(DBMS_RANDOM.VALUE(1, 50000)), 
    SYSDATE - TRUNC(DBMS_RANDOM.VALUE(1, 365)), 
    TRUNC(DBMS_RANDOM.VALUE(10, 1000), 2)
FROM 
    dual 
CONNECT BY 
    level <= 50000;

-- 生成 order_items 表的模拟数据
INSERT INTO order_items(order_item_id, order_id, product_id, quantity, price)
SELECT 
    rownum, 
    TRUNC(DBMS_RANDOM.VALUE(1, 50000)), 
    TRUNC(DBMS_RANDOM.VALUE(1, 50000)), 
    TRUNC(DBMS_RANDOM.VALUE(1, 10)), 
    TRUNC(DBMS_RANDOM.VALUE(10, 100), 2)
FROM 
    dual 
CONNECT BY 
    level <= 50000;

-- 生成 shipments 表的模拟数据
INSERT INTO shipments(shipment_id, order_id, shipped_date, carrier, tracking_number)
SELECT 
    rownum, 
    TRUNC(DBMS_RANDOM.VALUE(1, 50000)), 
    SYSDATE - TRUNC(DBMS_RANDOM.VALUE(1, 30)), 
    CASE MOD(rownum, 3) 
        WHEN 0 THEN 'UPS' 
        WHEN 1 THEN 'FedEx' 
        ELSE 'USPS' 
    END, 
    TRUNC(DBMS_RANDOM.VALUE(100000000, 999999999))
FROM 
    dual 
CONNECT BY 
    level <= 50000;

-- 生成 payments 表的模拟数据
INSERT INTO payments(payment_id, order_id, payment_date, amount)
SELECT 
    rownum, 
    TRUNC(DBMS_RANDOM.VALUE(1, 50000)), 
    SYSDATE - TRUNC(DBMS_RANDOM.VALUE(1, 30)), 
    TRUNC(DBMS_RANDOM.VALUE(10, 1000), 2)
FROM 
    dual 
CONNECT BY 
    level <= 50000;
