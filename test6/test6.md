﻿<!-- markdownlint-disable MD033-->
<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - 学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 实验步骤
- 创建新用户
```sql
 CREATE USER sales_user IDENTIFIED BY "123456";
-- DROP USER sales_user CASCADE;
```

- 授予用户权限
```sql
 GRANT CREATE SESSION TO sales_user;
 GRANT CREATE TABLE TO sales_user;
 GRANT CREATE PROCEDURE TO sales_user;
```

- 创建表空间
```sql
 CREATE TABLESPACE ts1 DATAFILE 'ts1.dbf' SIZE 1G;
 CREATE TABLESPACE ts2 DATAFILE 'ts2.dbf' SIZE 1G;
```

- 在表空间下建表
```sql
-- 在表空间 ts1 中创建 customers 表
CREATE TABLE customers (
    customer_id NUMBER(10) PRIMARY KEY,
    first_name VARCHAR2(50),
    last_name VARCHAR2(50),
    email VARCHAR2(100),
    phone VARCHAR2(20),
    address VARCHAR2(200)
) TABLESPACE ts1;

-- 在表空间 ts1 中创建 products 表
CREATE TABLE products (
    product_id NUMBER(10) PRIMARY KEY,
    name VARCHAR2(100),
    description VARCHAR2(200),
    price NUMBER(10, 2)
) TABLESPACE ts1;

-- 在表空间 ts1 中创建 orders 表
CREATE TABLE orders (
    order_id NUMBER(10) PRIMARY KEY,
    customer_id NUMBER(10),
    order_date DATE,
    total NUMBER(10, 2),
    CONSTRAINT fk_customer
        FOREIGN KEY (customer_id)
        REFERENCES customers(customer_id)
) TABLESPACE ts1;

-- 在表空间 ts2 中创建 order_items 表
CREATE TABLE order_items (
    order_item_id NUMBER(10) PRIMARY KEY,
    order_id NUMBER(10),
    product_id NUMBER(10),
    quantity NUMBER(10),
    price NUMBER(10, 2),
    CONSTRAINT fk_order
        FOREIGN KEY (order_id)
        REFERENCES orders(order_id),
    CONSTRAINT fk_product
        FOREIGN KEY (product_id)
        REFERENCES products(product_id)
) TABLESPACE ts2;

-- 在表空间 ts2 中创建 shipments 表
CREATE TABLE shipments (
    shipment_id NUMBER(10) PRIMARY KEY,
    order_id NUMBER(10),
    shipped_date DATE,
    carrier VARCHAR2(50),
    tracking_number VARCHAR2(50),
    CONSTRAINT fk_order_shipment
        FOREIGN KEY (order_id)
        REFERENCES orders(order_id)
) TABLESPACE ts2;

-- 在表空间 ts2 中创建 payments 表
CREATE TABLE payments (
    payment_id NUMBER(10) PRIMARY KEY,
    order_id NUMBER(10),
    payment_date DATE,
    amount NUMBER(10, 2),
    CONSTRAINT fk_order_payment
        FOREIGN KEY (order_id)
        REFERENCES orders(order_id)
) TABLESPACE ts2;
```

- 生成并插入模拟数据，每张表5万条，总计为30万条
```sql
-- 生成 customers 表的模拟数据
INSERT INTO customers(customer_id, first_name, last_name, email, phone, address)
SELECT 
    rownum, 
    'First' || rownum, 
    'Last' || rownum, 
    'email' || rownum || '@example.com', 
    '123-456-' || LPAD(rownum, 4, '0'), 
    'Address ' || rownum || ', City, State, Zip'
FROM 
    dual 
CONNECT BY 
    level <= 50000;

-- 生成 products 表的模拟数据
INSERT INTO products(product_id, name, description, price)
SELECT 
    rownum, 
    'Product ' || rownum, 
    'Description for product ' || rownum, 
    TRUNC(DBMS_RANDOM.VALUE(10, 1000), 2) 
FROM 
    DUAL 
CONNECT BY 
    level <= 50000;

-- 生成 orders 表的模拟数据
INSERT INTO orders(order_id, customer_id, order_date, total)
SELECT 
    rownum, 
    TRUNC(DBMS_RANDOM.VALUE(1, 50000)), 
    SYSDATE - TRUNC(DBMS_RANDOM.VALUE(1, 365)), 
    TRUNC(DBMS_RANDOM.VALUE(10, 1000), 2)
FROM 
    dual 
CONNECT BY 
    level <= 50000;

-- 生成 order_items 表的模拟数据
INSERT INTO order_items(order_item_id, order_id, product_id, quantity, price)
SELECT 
    rownum, 
    TRUNC(DBMS_RANDOM.VALUE(1, 50000)), 
    TRUNC(DBMS_RANDOM.VALUE(1, 50000)), 
    TRUNC(DBMS_RANDOM.VALUE(1, 10)), 
    TRUNC(DBMS_RANDOM.VALUE(10, 100), 2)
FROM 
    dual 
CONNECT BY 
    level <= 50000;

-- 生成 shipments 表的模拟数据
INSERT INTO shipments(shipment_id, order_id, shipped_date, carrier, tracking_number)
SELECT 
    rownum, 
    TRUNC(DBMS_RANDOM.VALUE(1, 50000)), 
    SYSDATE - TRUNC(DBMS_RANDOM.VALUE(1, 30)), 
    CASE MOD(rownum, 3) 
        WHEN 0 THEN 'UPS' 
        WHEN 1 THEN 'FedEx' 
        ELSE 'USPS' 
    END, 
    TRUNC(DBMS_RANDOM.VALUE(100000000, 999999999))
FROM 
    dual 
CONNECT BY 
    level <= 50000;

-- 生成 payments 表的模拟数据
INSERT INTO payments(payment_id, order_id, payment_date, amount)
SELECT 
    rownum, 
    TRUNC(DBMS_RANDOM.VALUE(1, 50000)), 
    SYSDATE - TRUNC(DBMS_RANDOM.VALUE(1, 30)), 
    TRUNC(DBMS_RANDOM.VALUE(10, 1000), 2)
FROM 
    dual 
CONNECT BY 
    level <= 50000;
```

- 脚本运行输出
```sql

User SALES_USER 已创建。


Grant 成功。


Grant 成功。


Grant 成功。


TABLESPACE TS1 已创建。


TABLESPACE TS2 已创建。


Table CUSTOMERS 已创建。


Table PRODUCTS 已创建。


Table ORDERS 已创建。


Table ORDER_ITEMS 已创建。


Table SHIPMENTS 已创建。


Table PAYMENTS 已创建。


50,000 行已插入。


50,000 行已插入。


50,000 行已插入。


50,000 行已插入。


50,000 行已插入。


50,000 行已插入。


```

- 在数据库中设计存储过程和函数
```sql
-- 声明
CREATE OR REPLACE PACKAGE my_package IS
  -- 插入数据的存储过程
  PROCEDURE insert_customer(p_id IN NUMBER, p_first_name IN VARCHAR2, p_last_name IN VARCHAR2, p_email IN VARCHAR2, p_phone IN VARCHAR2, p_address IN VARCHAR2);
  
  PROCEDURE insert_product(p_id IN NUMBER, p_name IN VARCHAR2, p_description IN VARCHAR2, p_price IN NUMBER);
  
  PROCEDURE insert_order(p_id IN NUMBER, p_customer_id IN NUMBER, p_order_date IN DATE, p_total IN NUMBER);
  
  PROCEDURE insert_order_item(p_id IN NUMBER, p_order_id IN NUMBER, p_product_id IN NUMBER, p_quantity IN NUMBER, p_price IN NUMBER);
  
  PROCEDURE insert_shipment(p_id IN NUMBER, p_order_id IN NUMBER, p_shipped_date IN DATE, p_carrier IN VARCHAR2, p_tracking_number IN VARCHAR2);
  
  PROCEDURE insert_payment(p_id IN NUMBER, p_order_id IN NUMBER, p_payment_date IN DATE, p_amount IN NUMBER);
  
  -- 获取数据的函数
  FUNCTION get_customers RETURN SYS_REFCURSOR;
  
  FUNCTION get_products RETURN SYS_REFCURSOR;
  
  FUNCTION get_orders RETURN SYS_REFCURSOR;
  
  FUNCTION get_order_items RETURN SYS_REFCURSOR;
  
  FUNCTION get_shipments RETURN SYS_REFCURSOR;
  
  FUNCTION get_payments RETURN SYS_REFCURSOR;
  
  -- 删除数据的存储过程
  PROCEDURE delete_customer(p_id IN NUMBER);
  
  PROCEDURE delete_product(p_id IN NUMBER);
  
  PROCEDURE delete_order(p_id IN NUMBER);
  
  PROCEDURE delete_order_item(p_id IN NUMBER);
  
  PROCEDURE delete_shipment(p_id IN NUMBER);
  
  PROCEDURE delete_payment(p_id IN NUMBER);
  
  -- 更新数据的存储过程
  PROCEDURE update_customer(p_id IN NUMBER, p_first_name IN VARCHAR2, p_last_name IN VARCHAR2, p_email IN VARCHAR2, p_phone IN VARCHAR2, p_address IN VARCHAR2);
  
  PROCEDURE update_product(p_id IN NUMBER, p_name IN VARCHAR2, p_description IN VARCHAR2, p_price IN NUMBER);
  
  PROCEDURE update_order(p_id IN NUMBER, p_customer_id IN NUMBER, p_order_date IN DATE, p_total IN NUMBER);
  
  PROCEDURE update_order_item(p_id IN NUMBER, p_order_id IN NUMBER, p_product_id IN NUMBER, p_quantity IN NUMBER, p_price IN NUMBER);
  
  PROCEDURE update_shipment(p_id IN NUMBER, p_order_id IN NUMBER, p_shipped_date IN DATE, p_carrier IN VARCHAR2, p_tracking_number IN VARCHAR2);
  
  PROCEDURE update_payment(p_id IN NUMBER, p_order_id IN NUMBER, p_payment_date IN DATE, p_amount IN NUMBER);
END my_package;
/



-- 实现
CREATE OR REPLACE PACKAGE BODY my_package IS
  PROCEDURE insert_customer(p_id IN NUMBER, p_first_name IN VARCHAR2, p_last_name IN VARCHAR2, p_email IN VARCHAR2, p_phone IN VARCHAR2, p_address IN VARCHAR2) IS
  BEGIN
    INSERT INTO customers (customer_id, first_name, last_name, email, phone, address) VALUES (p_id, p_first_name, p_last_name, p_email, p_phone, p_address);
    COMMIT;
  END;
  
  PROCEDURE insert_product(p_id IN NUMBER, p_name IN VARCHAR2, p_description IN VARCHAR2, p_price IN NUMBER) IS
  BEGIN
    INSERT INTO products (product_id, name, description, price) VALUES (p_id, p_name, p_description, p_price);
    COMMIT;
  END;
  
  PROCEDURE insert_order(p_id IN NUMBER, p_customer_id IN NUMBER, p_order_date IN DATE, p_total IN NUMBER) IS
  BEGIN
    INSERT INTO orders (order_id, customer_id, order_date, total) VALUES (p_id, p_customer_id, p_order_date, p_total);
    COMMIT;
  END;
  
  PROCEDURE insert_order_item(p_id IN NUMBER, p_order_id IN NUMBER, p_product_id IN NUMBER, p_quantity IN NUMBER, p_price IN NUMBER) IS
  BEGIN
    INSERT INTO order_items (order_item_id, order_id, product_id, quantity, price) VALUES (p_id, p_order_id, p_product_id, p_quantity, p_price);
    COMMIT;
  END;
  
  PROCEDURE insert_shipment(p_id IN NUMBER, p_order_id IN NUMBER, p_shipped_date IN DATE, p_carrier IN VARCHAR2, p_tracking_number IN VARCHAR2) IS
  BEGIN
    INSERT INTO shipments (shipment_id, order_id, shipped_date, carrier, tracking_number) VALUES (p_id, p_order_id, p_shipped_date, p_carrier, p_tracking_number);
    COMMIT;
  END;
  
  PROCEDURE insert_payment(p_id IN NUMBER, p_order_id IN NUMBER, p_payment_date IN DATE, p_amount IN NUMBER) IS
  BEGIN
    INSERT INTO payments (payment_id, order_id, payment_date, amount) VALUES (p_id, p_order_id, p_payment_date, p_amount);
    COMMIT;
  END;
  
  -- 获取数据的函数
  FUNCTION get_customers RETURN SYS_REFCURSOR IS
    r_cursor SYS_REFCURSOR;
  BEGIN
    OPEN r_cursor FOR
      SELECT * FROM customers;
    RETURN r_cursor;
  END;
  
  FUNCTION get_products RETURN SYS_REFCURSOR IS
    r_cursor SYS_REFCURSOR;
  BEGIN
    OPEN r_cursor FOR
      SELECT * FROM products;
    RETURN r_cursor;
  END;
  
  FUNCTION get_orders RETURN SYS_REFCURSOR IS
    r_cursor SYS_REFCURSOR;
  BEGIN
    OPEN r_cursor FOR
      SELECT * FROM orders;
    RETURN r_cursor;
  END;
  
  FUNCTION get_order_items RETURN SYS_REFCURSOR IS
    r_cursor SYS_REFCURSOR;
  BEGIN
    OPEN r_cursor FOR
      SELECT * FROM order_items;
    RETURN r_cursor;
  END;
  
  FUNCTION get_shipments RETURN SYS_REFCURSOR IS
    r_cursor SYS_REFCURSOR;
  BEGIN
    OPEN r_cursor FOR
      SELECT * FROM shipments;
    RETURN r_cursor;
  END;
  
  FUNCTION get_payments RETURN SYS_REFCURSOR IS
    r_cursor SYS_REFCURSOR;
  BEGIN
    OPEN r_cursor FOR
      SELECT * FROM payments;
    RETURN r_cursor;
  END;
  
  -- 删除数据的存储过程
  PROCEDURE delete_customer(p_id IN NUMBER) IS
  BEGIN
    DELETE FROM customers WHERE customer_id = p_id;
    COMMIT;
  END;
  
  PROCEDURE delete_product(p_id IN NUMBER) IS
  BEGIN
    DELETE FROM products WHERE product_id = p_id;
    COMMIT;
  END;
  
  PROCEDURE delete_order(p_id IN NUMBER) IS
  BEGIN
    DELETE FROM orders WHERE order_id = p_id;
    COMMIT;
  END;
  
  PROCEDURE delete_order_item(p_id IN NUMBER) IS
  BEGIN
    DELETE FROM order_items WHERE order_item_id = p_id;
    COMMIT;
  END;
  
  PROCEDURE delete_shipment(p_id IN NUMBER) IS
  BEGIN
    DELETE FROM shipments WHERE shipment_id = p_id;
    COMMIT;
  END;
  
  PROCEDURE delete_payment(p_id IN NUMBER) IS
  BEGIN
    DELETE FROM payments WHERE payment_id = p_id;
    COMMIT;
  END;
  
  -- 更新数据的存储过程
  PROCEDURE update_customer(p_id IN NUMBER, p_first_name IN VARCHAR2, p_last_name IN VARCHAR2, p_email IN VARCHAR2, p_phone IN VARCHAR2, p_address IN VARCHAR2) IS
  BEGIN
    UPDATE customers SET first_name = p_first_name, last_name = p_last_name, email = p_email, phone = p_phone, address = p_address WHERE customer_id = p_id;
    COMMIT;
  END;
  
  PROCEDURE update_product(p_id IN NUMBER, p_name IN VARCHAR2, p_description IN VARCHAR2, p_price IN NUMBER) IS
  BEGIN
    UPDATE products SET name = p_name, description = p_description, price = p_price WHERE product_id = p_id;
    COMMIT;
  END;
  
  PROCEDURE update_order(p_id IN NUMBER, p_customer_id IN NUMBER, p_order_date IN DATE, p_total IN NUMBER) IS
  BEGIN
    UPDATE orders SET customer_id = p_customer_id, order_date = p_order_date, total = p_total WHERE order_id = p_id;
    COMMIT;
  END;
  
  PROCEDURE update_order_item(p_id IN NUMBER, p_order_id IN NUMBER, p_product_id IN NUMBER, p_quantity IN NUMBER, p_price IN NUMBER) IS
  BEGIN
    UPDATE order_items SET order_id = p_order_id, product_id = p_product_id, quantity = p_quantity, price = p_price WHERE order_item_id = p_id;
    COMMIT;
  END;
  
  PROCEDURE update_shipment(p_id IN NUMBER, p_order_id IN NUMBER, p_shipped_date IN DATE, p_carrier IN VARCHAR2, p_tracking_number IN VARCHAR2) IS
  BEGIN
    UPDATE shipments SET order_id = p_order_id, shipped_date = p_shipped_date, carrier = p_carrier, tracking_number = p_tracking_number WHERE shipment_id = p_id;
    COMMIT;
  END;
  
  PROCEDURE update_payment(p_id IN NUMBER, p_order_id IN NUMBER, p_payment_date IN DATE, p_amount IN NUMBER) IS
  BEGIN
    UPDATE payments SET order_id = p_order_id, payment_date = p_payment_date, amount = p_amount WHERE payment_id = p_id;
    COMMIT;
  END;
END my_package;
/
```

- 备份方案

详细请见额外的markdown文档




## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|